import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image, Keyboard } from 'react-native';
import Constants from 'expo-constants';
import SwitchSelector from 'react-native-switch-selector';

const BmiCalculator = () => {
    const [unit, setUnit] = useState('us');
    const [weight, setWeight] = useState('');
    const [feet, setFeet] = useState('');
    const [inches, setInches] = useState('');
    const [height, setHeight] = useState('');
    const [bmi, setBmi] = useState('');
    const [description, setDescription] = useState('');
    const [resultColor, setResultColor] = useState('#3498db');

    const calculateBmi = () => {
        let heightInMeters;
        let weightInKg;

        if (unit === 'us') {
            const heightInInches = parseInt(feet) * 12 + parseInt(inches);
            heightInMeters = heightInInches * 0.0254;
            weightInKg = parseFloat(weight) * 0.453592;
        } else {
            heightInMeters = parseFloat(height) / 100;
            weightInKg = parseFloat(weight);
        }

        const bmiValue = (weightInKg / (heightInMeters ** 2)).toFixed(1);
        setBmi(bmiValue);

        if (bmiValue < 18.5) {
            setDescription('Underweight');
            setResultColor('#8ab1dc');
        } else if (bmiValue >= 18.5 && bmiValue < 25) {
            setDescription('Normal');
            setResultColor('#3cd364');
        } else if (bmiValue >= 25 && bmiValue < 30) {
            setDescription('Overweight');
            setResultColor('#FCD12A');
        } else if (bmiValue >= 30 && bmiValue < 35) {
            setDescription('Obese');
            setResultColor('#fb8030');
        } else {
            setDescription('Extremely Obese');
            setResultColor('#fb5253');
        }

        Keyboard.dismiss();
    }

    const resetFields = () => {
        setWeight('');
        setFeet('');
        setInches('');
        setHeight('');
        setBmi('');
        setDescription('');
    }

    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.titleText}>BMI Calculator</Text>
            </View>
            <SwitchSelector
                options={[
                    { label: 'U.S. units', value: 'us' },
                    { label: 'Metric', value: 'metric' }
                ]}
                initial={0}
                onPress={value => {
                    setUnit(value);
                    resetFields();
                }}
                style={styles.switchSelector}
            />
            <View style={styles.mainContent}>
                <View style={styles.inputContainer}>
                    {unit === 'us' ? (
                        <>
                            <TextInput
                                style={styles.input}
                                placeholder="Height (feet)"
                                keyboardType="numeric"
                                value={feet}
                                onChangeText={text => setFeet(text)}
                            />
                            <TextInput
                                style={styles.input}
                                placeholder="Height (inches)"
                                keyboardType="numeric"
                                value={inches}
                                onChangeText={text => setInches(text)}
                            />
                        </>
                    ) : (
                        <TextInput
                            style={styles.input}
                            placeholder="Height (cm)"
                            keyboardType="numeric"
                            value={height}
                            onChangeText={text => setHeight(text)}
                        />
                    )}
                    <TextInput
                        style={styles.input}
                        placeholder={unit === 'us' ? "Weight (lbs)" : "Weight (kg)"}
                        keyboardType="numeric"
                        value={weight}
                        onChangeText={text => setWeight(text)}
                    />
                </View>
                <TouchableOpacity
                    style={styles.button}
                    onPress={calculateBmi}
                >
                    <Text style={styles.buttonText}>Calculate</Text>
                </TouchableOpacity>
                {bmi !== '' && (
                    <View style={styles.resultView}>
                        <Text style={[styles.result, { color: resultColor }]}>{bmi}</Text>
                        <Text style={[styles.result, { color: resultColor }]}>{description}</Text>
                    </View>
                )}
            </View>
            {bmi !== '' && (
                <View style={styles.imageContainer}>
                    <Image
                        source={require('../assets/new_cdc_bmi_image.png')}
                        style={styles.resultImage}
                        resizeMode="contain"
                    />
                </View>
            )}
        </View>
    );
};

export default BmiCalculator;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#ecf0f1',
        alignItems: 'center',
    },
    title: {
        backgroundColor: '#3498db',
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: '100%',
    },
    titleText: {
        fontSize: 30,
        color: '#ffffff',
        fontWeight: 'bold',
    },
    mainContent: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
    },
    inputContainer: {
        width: '80%',
        marginVertical: 5,
    },
    input: {
        height: 50,
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        backgroundColor: '#cde0c9',
        fontSize: 20,
        marginBottom: 10,
        textAlign: 'center',
    },
    button: {
        height: 50,
        backgroundColor: '#3498db',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        borderRadius: 5,
        width: '80%',
    },
    buttonText: {
        color: '#ffffff',
        fontSize: 20,
    },
    resultView: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        width: '100%',
    },
    result: {
        fontSize: 27,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    imageContainer: {
        width: '100%',
        alignItems: 'center',
        padding: 10,
    },
    resultImage: {
        width: '80%',
        aspectRatio: 1,
    },
    switchSelector: {
        marginBottom: 15,
        width: '80%',
    },
});
