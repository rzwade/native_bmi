import { StyleSheet, View } from 'react-native';
import BmiCalcultaor from './src';


export default function App() {
  return (
    <View style={styles.container}>
      <BmiCalcultaor />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
